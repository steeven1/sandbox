// import { accountService } from '@/_services';

const facebookAppId = process.env.VUE_APP_FACEBOOK_APP_ID;

// export function initFacebookSdk() {
//     return new Promise(resolve => {
//         // wait for facebook sdk to initialize before starting the vue app
//         window.fbAsyncInit = function () {
//             FB.init({
//                 appId: facebookAppId,
//                 cookie: true,
//                 xfbml: true,
//                 version: 'v16.0'
//             });

//             // auto authenticate with the api if already logged in with facebook
//             FB.getLoginStatus(({ authResponse }) => {
//                 if (authResponse) {
//                     // accountService.apiAuthenticate(authResponse.accessToken).then(resolve);
//                 } else {
//                     resolve();
//                 }
//             });
//         };

//         // load facebook sdk script
//         (function (d, s, id) {
//             var js, fjs = d.getElementsByTagName(s)[0];
//             if (d.getElementById(id)) { return; }
//             js = d.createElement(s); js.id = id;
//             js.src = "https://connect.facebook.net/en_US/sdk.js";
//             fjs.parentNode.insertBefore(js, fjs);
//         }(document, 'script', 'facebook-jssdk'));    
//     });
// }



/// ===========================

const vue_fb = {}
vue_fb.install = function install(Vue, options) {
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0]
        if (d.getElementById(id)) {return}
        js = d.createElement(s)
        js.id = id
        js.src = "//connect.facebook.net/en_US/sdk.js"
        fjs.parentNode.insertBefore(js, fjs)
        console.log('setting fb sdk')
    }(document, 'script', 'facebook-jssdk'))

    window.fbAsyncInit = function onSDKInit() {
        FB.init(options)
        FB.AppEvents.logPageView()
        Vue.FB = FB
        vue_fb.sdk = FB // do not forget this line
        window.dispatchEvent(new Event('fb-sdk-ready'))
    }
    Vue.FB = undefined
}

import Vue from 'vue'

Vue.use(vue_fb, {
    appId: facebookAppId,
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v16.0'
})

// and this line
export default ({ app }, inject) => {
inject('fb', vue_fb)
}